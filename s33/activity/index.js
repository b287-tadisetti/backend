console.log("Helloo world!!");

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: 'GET'
})
.then((response) => response.json())
.then((json) => console.log(json.map(data => data.title)));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'GET'
})
.then((response) => response.json())
.then((json) => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => {console.log(`The item "${json.title}" on the list has a status of "${json.completed}"`)})


fetch("https://jsonplaceholder.typicode.com/todos", {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		userId:1,
		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
			title: "Updated To Do List Item",
			userId: 1,
			status: "Pending"
		})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			dateCompleted: "07/09/21",
			status: "Complete"
		})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'DELETE'
});