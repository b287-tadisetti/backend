console.log("Hello World!!");
/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only   display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
*/
	function add(numA, numB){
		let sum = numA+numB;
		console.log("Dispalyed sum of "+numA+" and "+numB);
		console.log(sum);
	};

	let a=5;
	let b=15;
	add(a, b);

	function subtract(numA, numB){
		let minus = numA-numB;
		console.log("Displayed difference of "+numA+" and "+numB);
		console.log(minus);
	};

	let c=20;
	let d=5;
	subtract(c,d);

/*
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.
*/

	function multiplication(numC, numD){
		let value = numC*numD;
		return value;
	};

	let numC=50;
	let numD=10;

	let product = multiplication(numC, numD);
	console.log("The product of "+numC+" and "+numD+":")
	console.log(product);

	function division(numC, numD){
		return numC/numD;
	};

	let quotient = division(numC, numD);
	console.log("The quotient of "+numC +" and "+numD+":");
	console.log(quotient);
/*
	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/

	function totalArea(radius){
		let result= 3.1416*(radius**2);//π=3.14159=3.1416
		return result;
	};

	let circleArea = totalArea(15);
	console.log("The result of getting the area of a circe with 15 radius:");
	console.log(circleArea);

/*

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
*/

	function average(num1, num2, num3, num4){
		let value = (num1+num2+num3+num4)/4;
		return value;
	};

	let averageVar = average(20, 40, 60, 80);
	console.log("The average of 20,40,60 and 80:");
	console.log(averageVar);
/*
	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


function checkIfPassed(score, total){
	console.log("Is 38/50 a passing score?");
	let isPassed= (score/total)*100 > 75;
	return isPassed;
}

let isPassingScore = checkIfPassed(38,50);

console.log(isPassingScore);