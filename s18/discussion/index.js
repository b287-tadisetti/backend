console.log("Hellow World?");

//[ SECTION ] Functions

	//[SUB-SECTION ] Parameters and Arguements

	//Functions in JS ar lines/blocks of codes that tell our device/application/browser to perform a certain task when called/invoked/triggered.

	function printInput(){
		let nickname = prompt("Enter your nicknmae:");
		console.log("Hi, "+nickname);
	};

	printInput();

	//For other cases, functions can also process data directly passed into it instead of relying only on Global Variable and prompt().

	//name ONLY ACTS LIKE A VARIABLE

	function printName(name){//name is parameter
		
		//This one should work as well:
		//let name1=name;
		//console.log("Hi, "+name1);

		console.log("Hi, "+name);
	};

	//console.log(name); //This one is error as it is not defined.

	printName("Henna"); //"Henna" is argument.

	//You can directly pass data into the function. The function can then call/use that data which is referrred as "name" within the function.

	printName("Carlo");

	//When the "printName()" function is called again, it stores the value of "Carlo" in the parameter "name" then uses it to print a message.

	//Variables can also be passed as an arguement.
	let sampleVariable = "Yuvi";

	printName(sampleVariable);

	//Function arguements cannot be used by a function if there are no parameters provided within the function.

	printName(); //It will be Hi, undefined

	function checkDivisibilityBy8(num){
		let remainder = num%8;
		console.log("The remainder of "+num+" divided by 8 is: "+remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is "+num+" divisible by 8?");
		console.log(isDivisibleBy8);
	};

	checkDivisibilityBy8(12);

	checkDivisibilityBy8(32);
	//You can also do the same using prompt(), however take note that prompt() outputs a string. Strings 

//Functions as Arguements

	//Function parameters can also accept other function as arguements

	function arguementFunction(){
		console.log("This function was passed as an arguement before the message was printed.");
	};

	/*function invokeFunction(arguementFunction){
		arguementFunction();
	};*/

	function invokeFunction(iAmNotRelated){
		//console.log(iAmNotRelated);
		iAmNotRelated();
		arguementFunction();
	}

	invokeFunction(arguementFunction);

	//Adding and removing the parenthese "()" impacts the output of JS heavily.

	console.log(arguementFunction);
	//will provide information about a function in the console using console.log();

//Using multiple parameters

	//Multiple "arguemnets" will correspond to the number of "parmeters" declared in a function n succeeding order.

		function createFullName(firstName, middleName, lastName){
			console.log(firstName+" "+middleName+" "+lastName);

			console.log(middleName+" "+lastName+" "+firstName);
		};

		createFullName("Mike", "Kel", "Jordan");

		createFullName("Mike", "Jordan");

		createFullName("Mike", "Kel", "Jordan", "Duo");

		//In Js, providing more/less arguements than the expected parameters will not return an error.

		//Using Variable as Arguements

		let firstName="Dhyan";
		let secondName="Rock";
		let lastName="John";

		createFullName(firstName, secondName, lastName);

	//[ SECION ] The Return Statement

		//The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

		function returnFullName(firstName, middleName, lastName){
			console.log("This message is from console.log");
			return firstName+" "+middleName+" "+lastName;
			console.log("This message is from console.log");
		};

		let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
		console.log(completeName);

		//In our example, the "returnFullNmae" function was invoked/called in the same line as deaclaring a variable.

		//Whatever value is returned from the "returnFullNmae" function is stored in the "completeName" variable.

		//Notice that the console.log() after the return is no longer printed because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.

		//In this example console.log() will print the returned value of the returnFullName() function.
		console.log(returnFullName(firstName, secondName, lastName)); 

		//You can also create a variable inside the function to contain the result and return that variable instead.

		function returnAddress(city, country){
			let fullAddress = city+", "+country;
			return fullAddress;
		};

		let myAddress = returnAddress("Manila City", "Philippines");
		console.log(myAddress);

		//On the otherhand, when a function only has console.log() to display its result it will be undefined instead.

		function printPlayerInfo(username, level, job){
			console.log("Username: "+username);
			console.log("Level: "+level);
			console.log("Job: "+job);
		};

		let user1 = printPlayerInfo("knight_white", 95, "Paladin");
		console.log(user1);

		//Returns undefined because printPlayerInfo return nothing. It only console.log the details.

		//you cannot save any value from printPlayerInfo() because it does not return anything.

