console.log("Hello World!!");

//conditional Statement
	//Allows us to control the flow of our program.It allows us to run a statement/instruction if a condition is met or run another seperate instruction if otherwise.

//[SECTION ] If , Else If, and Else Statement
	
	//If Statements
	
	let numA = -1;

	if(numA <= 0){
		console.log("Hello");
	};

	//if numA is less than 0, run console.log("Hello");

	/*
	Syntax:

		if(condition){
			statement;
		};
	*/

	console.log(numA <= 0); //results will be true.

	//The result of expression added in the if's condition must result to true, else the statement inside the if() will not run.

	//Let's update the variable and run an if statement with the same condition.

	numA = -1;

	if(numA>0){
		console.log('Hellow again from numA is -1');
	};

	//this will not run because the expression now results to false;
	console.log(numA > 0);

	let city = "New York";

	if(city === "New York"){
		console.log('Welcome to New York City!');
	};

	console.log(city == "New York");

//Else If Clause

	//Executes a statement if previous conditions are false and if the specified condition is true

	let numH = 1;

	numA=1;

	if(numA < 0){// the condition is now false, if false code block
		console.log("Hellow");
	}else if(numH > 0){// the condition now here is true, thus this will print console.log("Wurld?");
		console.log("Wurlld?");
	};

	//We were able to run the else if() statement after we evaluated the if condition is false.

	//If the if() condition was passed and run, we will no longer evaluate to else if() and end the process there.

	let numB = 5;

	if(numB<0){
		console.log("This will not run!");
	}else if(numB<5){
		console.log("The value is less than 5");
	}else if(numB<10){
		console.log("The value is less than 10");
	};

	//let's now update the city variable, and look at another example

	city ="Tokyo";

	if(city === "New York"){
		console.log("Welcome to New York City!");
	}else if(city === 'Tokyo'){
		console.log("Welcome to Tokyo, Japan!");
	};

//Else Statement
	
	//Execute a statement if all existing conditions are false

	if(numA < 0){
		console.log("Hello!");
	}else if(numH===0){
		console.log('World');
	}else{
		console.log("Again");
	};

	//Since both the preceding if and else if conditions failed, the else statement was run instead.

	numB = 21;

	if(numB < 0){
		console.log("This will not run!");
	}else if(numB < 5){
		console.log("The value is less than 5");
	}else if(numB < 10){
		console.log("The value is less than 10");
	}else if(numB < 15){
		console.log("The value is less than 15");
	}else if(numB < 20){
		console.log("The value is less than 20");
	}else{
		console.log("The value is greater than 20!");
	};

	/*else{
		console.log("Do this will run?");
	};

	//In fact, it results to an error.

	else if(numH === 0){
		console.log("World");
	}else{
		console.log('How about this one?');
	};*/

	//Same goes foe else if, there should be a preceding if() first.

//if, else if, and else statements with functions

	//Most of the times we would like to use if, else if, and else statement with functions to control the flow of our application.

	let message = "No Message";
	console.log(message);

	function determineTyphoonIntensity(windSpeed){

		if(windSpeed < 30){
			return 'Not a typhoon yet!';
		}
		else if(windSpeed <= 61){
			return 'Tropical Depression Detected';
		}else if(windSpeed >= 62 && windSpeed <=88){
			return 'Tropical storm Detected';
		}else if(windSpeed >= 89 || windSpeed<=117){
			return 'Severe Tropical Storm Detected';
		}else if(windSpeed > 118){
			return 'Typhoon Detected';
		}else{
			return 'Typhoon detected! Keep Safe!';
		};
	};

	message = determineTyphoonIntensity(95);
	console.log(message);

//Truthy and Falsy

	//In JS a truthy value is a value that is considered true when encountered in a Boolean context

	/*
	Falsy values/Exception for truhy
	1.False
	2.0
	3.-0
	4.""
	5.null
	6.undefined
	7.Nan
*/

	if(true){
		console.log("Truthy!");
	};
	if(1){
		console.log("Truthy!");
	};
	if([]){
		console.log("Truthy!");
	};

	//False Example
	if(false){
		console.log("Falsy!");
	};

	if(0){
		console.log("Falsy!");
	};

	if(undefined){
		console.log("Falsy!");
	};

//Conditional (Tenary) Operaor

	//The Conditional ternary Operator takes in three operands:
		//1.Condition
		//2.Expression to execute if the condition is truthy
		//3.expression to execute if the condition is falsy
	//Can be used as an alternative to an "if else" statement

	/*
	Syntax:
		(expression) ? ifTrue : ifFalse;
	*/

//Single Ternary Statement Execution
	let ternaryResult = (1<18) ? true : false;
	//let ternaryResult = (1<18) ? false : true;//Will result to false
	console.log('Result of Ternary Operator: '+ternaryResult);

//multiple Statement Execution

	let name;

	function isOfLegalAge(){
		name = 'John';
		return 'You are of the legal age limit';
	};

	function isunderAge(){
		name = 'Jane';
		return 'You are under the age limit';
	};

	let age = parseInt(prompt('What is your age?'));
	console.log(age);
	console.log(typeof age);
	let legalAge=(age>18)?isOfLegalAge():isunderAge();
	console.log('Result of Tenary Operator in functions: '+legalAge+", "+name);

//Switch Statement

		//The switch statement evaluates an expression and matches the expression's value to a case clause.
		//The "toLowerCase()" is a function/method that will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions.

		/*
		Syntax:
			switch(expression){
				case value:
					statement;
					break;
				default:
					statement;
					break;
			}
		*/

		let day = prompt('What day of the week is it today?').toLowerCase();

		console.log(day);

		switch(day){
			case 'monday':
				console.log('The color of the day is red!');
				break;
			case 'tuesday':
				console.log('The color of the day is blue!');
				break;
			case 'wednesday':
				console.log('The color of the day is orange!');
				break;
			case 'thursday':
				console.log('The color of the day is violet!');
				break;
			case 'friday':
				console.log('The color of the day is yellow!');
				break;
			case 'saturday':
				console.log('The color of the day is pink!');
				break;
			default :
				console.log('Please input a valid day!');
				break;
		}

	//Try-Catch-Finally Statement
		//try-catch statements are commonly used for error handing

		function showIntensityAlert(windSpeed){
			//insert try-catch-finally instead
			try {
				alerts(determineTyphoonIntensity(windSpeed));
			}catch(error){
				console.log(typeof error);
				console.log(error);

				console.warn(error.message);
				console.log(error.message);
			}finally {
				alert('Intensity updates will show new alert!');
				console.log('This is from finally!');
			}

			//Continue execution of code regardless of success and failure of code execution in the "try" block to handle/
		}

		showIntensityAlert(56);