const Task = require("../models/task");

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(error);
			return false;
		}
		else if(result!= null){
			//result.status = "complete"
			return result;
		};
		

	});
};


module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((result, err) => {
		if(err){
			console.log(error);
			return false;
		} else{
			return result;
		}
	});
};

module.exports.updateTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} else{
			result.status = "complete";
			return result.save().then((updatedTask, saveErr) => {
				if(saveErr){
					console.log(saveErr);
					return false;
				} else{
					return result;
				}
			})
		};
	});
};
