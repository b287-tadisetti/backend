console.log("Hello World");

let firstName = "Hema";
console.log("First Name: "+firstName);

let lastName = "Sri";
console.log("Last Name: "+lastName);

let age = 19;
console.log("Age: "+age);

let hobbies = ["Singing","Dancing","Playing"];
console.log("Hobbies:");
console.log(hobbies);

let workAddress = {
	houseNumber: "7-43",
	street: "Bapu Street",
	city: "vizag",
	state: "AP"
}
console.log("Work Address:");
console.log(workAddress);

//Debugged code

let fullName = "Steve Rogers";
console.log("My full name is: "+ fullName);

let currentAge = 40;
console.log("My current age is: "+ currentAge);

let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log("My friends are: ")
console.log(friends);

let profile = {
	username: "captain-america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false
}
console.log("My Full Profile: ")
console.log(profile);

let bestfriendName = "Bucky Barnes";
console.log("My bestfriend is: " + bestfriendName);

const lastLocation = "Arctic Ocean";
//lastLocation = "Atlantic Ocean";
console.log("I was found frozen in: " + lastLocation);

