console.log("Hellow world?");

// [SECTION ] Syntax, Statements and Comments
	//JS Statements usually end with semicolon (;)
	// JS Statements in programming are instruction that we tell the computer/ browser to perform.
	//A syntax in programming is the set of rules that describes how statements must be constructed.

	/*
	There are two types of comments:
	1.The single-lone comment denotes by two slashes //
	2.The multi-lo=ine comment denote by a slash and asterisk
	*/

// [ SECTION ] Variables
	//It is used to contain data
	//Like a storage, container
		//When we create variables, certain portions of a device's memory is given a "name" that we cal "variables"

	//[ SUB_SECTION ] Declaration Variables
	//Tells our deice/local machine that a variable name is created and is ready to store data.
	//Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning that the variable's value was not yet defined.
		
		//Syntax
			//let/const variableName;
		let myVariable;

		console.log(myVariable);
		//prints the value of myVariable in the console.

		/*
		Guide in writing variables:
			1.Use the "let" keyword followed by the variable name of your choosing and use the assignment operator (=)to assign the value 
			2.Variable names should start with a lowercase character, or als known for camelCase.
				Ex.
				favColor
				favoriteNumber
				collegeCourse
			3.For constant variable, use the "const" keyword
			4.Variable names should be indicative (or descriptive ) of the value being stored to avoid confusion.
		*/

	//[ SUB-SECTION] Intializing Variables
		// The instance when a variable is given it's initial/starting value

		//Sntax
			//let/const varaiableName= value;
		let productName = "desktop computer";
		console.log(productName);

		let productPrice=18999;
		console.log(productPrice);

		//Usually being when a certain value is not changing.
		//More likely being used for interest rate for a loan , saving account or a mortgage.
		const interest = 3.539;
		console.log(interest);

	// [ SB_ SECTION ] Reassigning Variable Values
		// Reassigning a variable pertains to changing it's initial or previous value into another value.

		//syntax
			//variable = newValue;
		productName = "Laptop";
		console.log(productName);

		//interest = 4.456;
		//console.log("Hellow World?");

	//[ Var vs. let/const ]
		//var is also used in declaring a variable., but var is an ECMASCript1 (ES1) feature, in modern time, ES6 Updates are already now using let/const.

		//For example,
		a = 5;
		console.log(a);
		var a;

		//let keyword
			//b=5;
			//console.log(b);
			//let b;

	//[ SUB-SECTION] let/const local/global scop
		//Scope essentially means where these variables are available for use
		//A block is a chunk of code bounded by {}. A block live in curly braces. Anything within curly braces is a block.

			let outerVariable = "hello";

			{
				let innerVariable ="hellow, again ";
				console.log(innerVariable);
			}

			console.log(outerVariable);
			
	//[ SUB_SECTION ] Multiple Variable declarations 
			// Multiple variables may be declared in one line.

			let productCode = 'DC017', productBrand ='Dell';
			console.log(productCode, productBrand);

//[ SECTION ] Data Types

	//[ Stringgs ]
		// String are a series of characters that create a word, a phrase, a sentence or anything related to creating text.
			//We use either single('') or double ("") quotations.

			let country = "Philippines";
			let province = "Metro Manila";

			//Concatenating Strings
			//Multiple strings values can be combined to create a single string using the "+" symbol

			let fullAddress = province +", "+country;
			console.log(fullAddress);

			let greetings = "I live in the " + country;
			console.log(greetings);

			//Escape Character (\) in string in combination wth ther characters can produce differeent effects

			//"\n" refers to creating a new line in between text
			let mailAddress = "Metro Manila\n\nPhilippines";
			console.log(mailAddress);

			let message = "John's employees went home early";
			console.log(message);
			message = 'John\'s employees went home early';
			console.log(message);

		// [ Numbers ]
			// Integers/Whole Numbers
			let headcount = 26;
			console.log(headcount);

			//Decimal Numbers/Fractions
			let grade = 98.7;
			console.log(grade);

			//Exponential Notation
			let planetDistance = 2e10;
			console.log(planetDistance);

			// Combining text and strings
			console.log("John's grade last quarter is " + grade);

		//[ Boolean ]
			//Boolean values are normally used to store values relating to the state of certain things
			//This is the, true or false

			let isMarried = false;
			let inGoodConduct = true;

			console.log("isMarried: " + isMarried);
			console.log("inGoodConduct:" + inGoodConduct);
		//[ Arrays ]
			//Arrays are a special kind of data type that's used to store multiple values

			//Syntax
			//let/const arrayName = [ elementA,elementB, elementC, ..]

			let grades = [98.7, 92.2, 90.6, 94.6];
			console.log(grades);

			//different data types
			let details = ["John", "Smith", 32, true];
			console.log(details);

		//[ Object]
			//Objects are another special kind of data type that's used to mimic real world objects/items

			//Syntax
			//let/const objectName = {
			//		propertyA: value,
			//		propertyB: value
			// }

			let person = {
				fullName: "Michael Jordan",
				age: 35,
				isMarried: true,
				contact: ["12345", "67890"],
				address: {
					houseNumber: "345",
					city:"Chicago"
				}
			}

			console.log(person);

			//typeof operator is uesd to determine the type of data or value of a variable. It outputs a string.
			console.log(typeof grades);

			//Constant Objects and Arrays

			/*
			The keyword const is a little misleading 

			It does not define a constant value. It defines a constant reference to a value.

			Because of this you can not:
				Reassign a constant value.
				Reassign a constant array.
				Reassign a constant object.

				But you can:

				Change the elements of constant array
				Change the properties of constant object
			*/
				//const anime = ['one piece', 'one punch man', 'attack on titan']
				//anime = ['kimetsu no yaiba']

				//console.log(anime);

				//but if we try to do this,
				const anime = ['one piece', 'one punch man', 'attack on titan']
				anime[0] = 'kimetsu no yaiba';

				console.log(anime);

			//[ SECTION ] Null
				//It is used to intentionally express the absence of a value in a variable declaration/intialization.
				//If we use the null value, simple means that a data type was assigned to a variable but it does not hold any value/amount or is nullified

				let spouse = null;
				let myNumber = 0;
				let myString = '';

				console.log(spouse);
				console.log(myNumber);
				console.log(myString);


