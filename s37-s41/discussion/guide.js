session s37:
	touch index.js .gitignore guide.js
	npm init --y
	npm install express mongoose cors
	create our Course Model
	created our User Model

session s38:
	create a route for checkEmail
	create a route for registerEmail
	npm install bcrypt
		use bcrypt in users password
	npm install jsonwebtoken
	create a route for user authentication
	create a route for user details

session s39:
	refactor the user details, added authorization
	create a route for add course

session s40:
	create a route for retrieving all the courses
	create a route for retrieving all active courses
	create a route for retrieving specific courses
	create a route for updating a course
	create a route for archiving a course 

session s41:
	create a route for enroll a course