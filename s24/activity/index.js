console.log("Hello World?");

let getCube = 4 ** 3;
console.log(getCube);

//using Template Literals
console.log(`The cube of 4 is ${getCube}`);

let address = [ '258 Washington Ave NW','California', 90011];

let [city, place, pin ] = address;

console.log(`I live at  ${city}, ${place} ${pin}`);

let animal = {
	name: 'Lolong',
	weight: 1075,
	length: 20,
	width: 3
};

let {name, weight, length, width} = animal;
console.log(`${name} was a saltwater crocodile. He weighed at ${weight} kgs with a measurement of ${length} ft ${width} in.`);

let numbers = [1,2,3,4,5];
numbers.forEach((number) => {
	console.log(number);
});

const reduceNumber = numbers.reduce((x,y) =>
{
	return x+y;
});


console.log(reduceNumber);
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);

