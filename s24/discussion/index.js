console.log("Hello World!!");

//[ SECTION ] Exponent Operator
	
	const firstNum = 8 ** 2;
	console.log(firstNum);

	const secondNum = Math.pow(8,2);
	console.log(secondNum);

//[ SECTION ] Template Literals

	//Allows us to write string without using the concatenation operator (+)

	let name = 'John';

	//Uses single quotes ('')
	let message = 'Hello '+name+"! Welcome to programming!";
	console.log(message);

	//Uses backticks(``)
	message = `Hello ${name}! Welcome to programming!`;
	console.log(`Message without template literals: ${message}`);

	//Multi-line using Template Literals
	const anotherMessage = `
	${name} attended a math competition.
	He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`

	console.log(anotherMessage);

		// Template literals allows us to write string with embedded JS expressions

	const interestRate = .1;
	const principal = 1000;

	console.log(`The interst on your saving account is: ${principal * interestRate }.`);

	//[ SECTION ] Array Destructuring
		//Allows us to unpack elements in arrays into distinct variables

		//Syntax:
			// let/const [ variableNmae, variableName, variableName ] = array;

		const fullName = ['Juan', 'Dela', 'Cruz' ];

		console.log(fullName[0]);
		console.log(fullName[1]);
		console.log(fullName[2]);

		console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}1 It's nice to meet you!`);

		const [ firstName, middleName, lastName ] = fullName;

		console.log(firstName);
		console.log(middleName);
		console.log(lastName);

		console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

	//[ SECTION ] Object Destructuring
		// Allows to unpack properties of objects into distinct variables

		//Syntax:
			//let/consst { propertyName, propertyName, propertyName } = object;

		const person = {
			givenName: 'Jane',
			maidenName: 'Dela',
			familyName: 'Cruz'
		};

		console.log(person.givenName);
		console.log(person.maidenName);
		console.log(person.familyName);

		console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`);

		//Object Destructuring
		const { givenName, maidenName, familyName } = person;

		console.log(givenName);
		console.log(maidenName);
		console.log(familyName);

		console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!}`);

		function getFullNmae ({givenName, maidenName, familyName}){
			console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
		};

		getFullNmae(person);
		console.log(person);

	// [ SECTION ] Arrow Functions
		// Compact alternative synatx to traditional functions

		const hello = () => {
			console.log('Hellow Wurld');
		};

		//hello(); //prints Hellow Wurld

		const printFullName = ( givenName, maidenName, familyName) => {
			console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);
		};

		printFullName(givenName, maidenName, familyName);
		printFullName(person);

		printFullName('John', 'D', 'Smith');

		const students = ['John', 'Jnae', 'Judy'];

		//Arrow Functions with loops
			//Pre-Arrow Function
		students.forEach(function(student){
			console.log(`${student} is present.`);
		});

		//Arrow Functions
		students.forEach((student) =>
		{
			console.log(`${student} is absent.`);
		});

	// [ SECTION ] Implicit Return Statement
		//There are instance when you can omit the 'return' statement


		//Pre-Arrow Function
		//const add = (x,y) => {
		//	return x+y;
		//}

		//let total = add(1,2);
		//console.log(total);

		//Arrow Function
		const add = (x,y) => x+y;

		let total = add(1,2);
		console.log(total);

// [ SECTION ] Default Function Arguement Value
	//Provides a default arguement value if none is provided when the function is invoked

	const greet = (name = 'User') => {
		return `Good morning, ${name}!`;
	};

	console.log(greet());
	console.log(greet('hema'));

// [ SECTION ] Class-Based Projct Blueprints
	//Allows creation/instantiation of object using classes as blueprints

	//Creating A Class
		//The constructor is a special method of a class for creating/initializing an object for that class.
		//Template for object
		//Syntax:
			//class classNmae {
				// constructor(objectPropertyA, objectPropertyB){
				//	this.objectPropertyA = objectPropertyA;
				// 	this.objectPropertyB = objectPropertB;
				//}
			//}

	class Car {
		constructor(brand, name, year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	//Instantiating an Object
		// The "new" opeartor creates/instantiates a new object with the given arguements as the values of it's properties

	let myCar = new Car();

	console.log(myCar);

	myCar.brand = "Ford";
	myCar.name = "Mustang";
	myCar.year = 2023;

	console.log(myCar);

	const myNewCar = new Car('Toyota','Hilux', 2020);
	console.log(myNewCar);

	console.log(typeof myNewCar);