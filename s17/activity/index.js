console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	let name=prompt("What is your name?");
	console.log("Hello, "+ name);

	let age=prompt("How old are you?");
	console.log("You are "+age+" years old.");

	let place=prompt("Where do you live?");
	console.log("You live in "+place+" City");

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favouriteBands(){
		console.log("1.The Beatles");
		console.log("2.Metallica");
		console.log("3.The Eagles");
		console.log("4.L'arc~en~Ciel");
		console.log("5.Eraserheads");
	}

	favouriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favouriteMovies(){
		let firstMovie="The Godfather";
		console.log("1."+firstMovie);
		console.log("Rotten Tomatoes Rating: 97%");

		let secondMovie="The Godfather, Part II";
		console.log("2."+secondMovie);
		console.log("Rotten Tomatoes Rating: 96%");

		let thirdMovie="Shawshanl Redemption";
		console.log("3."+thirdMovie);
		console.log("Rotten Tomatoes Rating: 91%");

		let fourthMovie="To Kill A Mockingbird";
		console.log("4."+fourthMovie);
		console.log("Rotten Tomatoes Rating: 93%");

		let fifthMovie="Psycho";
		console.log("5."+fifthMovie);
		console.log("Rotten Tomatoes Rating: 96%");
	}

	favouriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	
let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

//console.log(friend1);
//console.log(friend2);


